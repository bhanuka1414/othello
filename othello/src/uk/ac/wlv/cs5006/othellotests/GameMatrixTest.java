package uk.ac.wlv.cs5006.othellotests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import uk.ac.wlv.cs5006.othello.GameMatrix;
import uk.ac.wlv.cs5006.othello.GameMatrixCell;
import uk.ac.wlv.cs5006.othello.GameMatrixLocation;
import uk.ac.wlv.cs5006.othello.GamePiece;

class GameMatrixTest {

	@Test
	void testConstructor() {
		 boolean work = true;
		    try {
		    	int nrows = 5;
		    	int ncols = 7;
		    	GameMatrix gameMatrix = new GameMatrix(nrows, ncols);
		    	assertTrue(nrows == gameMatrix.getNumRows());
		    	assertTrue(ncols == gameMatrix.getNumColumns());
		    }catch(Exception e){
		    	work = false;
		    }
		    assertTrue(work);
	}
	
	@Test
	void testSettersAndGetters() {
		int nrows = 5;
    	int ncols = 7;
    	GameMatrix gameMatrix = new GameMatrix(nrows, ncols);
    	
    	int row = 4;
    	int col = 6;
    	String value = "BLACK";
    	assertTrue(gameMatrix.setGamePieceValue(row, col, value));
    	
    	assertTrue(gameMatrix.getGamePiece(row, col).getValue().equals(value));
    	
    	assertTrue(nrows == gameMatrix.getNumRows());
    	assertTrue(ncols == gameMatrix.getNumColumns());
    	
	}
	
	@Test
	void testClear() {
		int nrows = 5;
    	int ncols =	5;
    	GameMatrix gameMatrix = new GameMatrix(nrows, ncols);
    	
    	int row = 4;
    	int col = 4;
    	String value = "BLACK";
    	assertTrue(gameMatrix.setGamePieceValue(row, col, value));
    	
    	int row2 = 3;
    	int col2 = 3;
    	String value2 = "WHITE";
    	assertTrue(gameMatrix.setGamePieceValue(row2, col2, value2));
    	
    	gameMatrix.clear();
    	
    	String emptyVal = "EMPTY";
    	assertTrue(gameMatrix.getGamePiece(row, col).getValue().equals(emptyVal));
    	assertTrue(gameMatrix.getGamePiece(row2, col2).getValue().equals(emptyVal));
    	
	}
	
	@Test
	void testIsOnGameMatrix() {
		int nrows = 5;
    	int ncols =	5;
    	GameMatrix gameMatrix = new GameMatrix(nrows, ncols);
    	
    	int row = 4;
    	int col = 4;
    	String value = "BLACK";
    	assertTrue(gameMatrix.setGamePieceValue(row, col, value));
    	
    	int row2 = 6;
    	int col2 = 6;
    	String value2 = "WHITE";
    	assertFalse(gameMatrix.setGamePieceValue(row2, col2, value2));
	}
	
	@Test
	void testGameMatrixIsFull() {
		int nrows = 2;
    	int ncols =	2;
    	GameMatrix gameMatrix = new GameMatrix(nrows, ncols);
    	
    	int row = 0;
    	int col = 0;
    	String value = "BLACK";
    	gameMatrix.setGamePieceValue(row, col, value);
    	assertFalse(gameMatrix.gameMatrixIsFull());
    	
    	gameMatrix.setGamePieceValue(0, 1, "BLACK");
    	gameMatrix.setGamePieceValue(1, 0, "BLACK");
    	gameMatrix.setGamePieceValue(1, 1, "BLACK");
    	
    	assertTrue(gameMatrix.gameMatrixIsFull());
	}
	
	@Test
	void testGameMatrixHasSingleValue() {
		int nrows = 2;
    	int ncols =	2;
    	GameMatrix gameMatrix = new GameMatrix(nrows, ncols);
    	
    	String value = "BLACK";
    	gameMatrix.setGamePieceValue(0, 0, value);
    	gameMatrix.setGamePieceValue(0, 1, value);
    	gameMatrix.setGamePieceValue(1, 0, value);
    	gameMatrix.setGamePieceValue(1, 1, value);
    	assertTrue(gameMatrix.gameMatrixHasSingleValue());
    	
    	
    	
    	GameMatrix gameMatrix2 = new GameMatrix(nrows, ncols);
    
    	gameMatrix2.setGamePieceValue(0, 0, value);
    	gameMatrix2.setGamePieceValue(0, 1, value);
    	gameMatrix2.setGamePieceValue(1, 0, "WHITE");
    	gameMatrix2.setGamePieceValue(1, 1, value);
    	assertFalse(gameMatrix2.gameMatrixHasSingleValue());
    	
    	
	}

}
