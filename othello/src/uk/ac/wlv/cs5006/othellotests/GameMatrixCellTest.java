package uk.ac.wlv.cs5006.othellotests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import uk.ac.wlv.cs5006.othello.GameMatrixCell;
import uk.ac.wlv.cs5006.othello.GameMatrixLocation;
import uk.ac.wlv.cs5006.othello.GamePiece;

class GameMatrixCellTest {

	@Test
	void testConstructor() {
		 boolean work = true;
		    try {
		    	int row = 5;
		    	int col = 7;
		    	GamePiece token = new GamePiece("WHITE");
		    	GameMatrixCell gameMatrixCell = new GameMatrixCell(row, col, token);
		    	assertTrue(token.equals(gameMatrixCell.getGamePiece()));
		    	assertTrue(row == gameMatrixCell.getLocation().getRow() && col == gameMatrixCell.getLocation().getCol());
		    	
		    	row = 8;
		    	col = 1;
		    	GamePiece token2 = new GamePiece("BLACK");
		    	GameMatrixCell gameMatrixCell2 = new GameMatrixCell(row, col, token2);
		    	assertTrue(token2.equals(gameMatrixCell2.getGamePiece()));
		    	assertTrue(row == gameMatrixCell2.getLocation().getRow() && col == gameMatrixCell2.getLocation().getCol());
		    	
		    }catch(Exception e){
		    	work = false;
		    }
		    assertTrue(work);
	}
	
	@Test
	void testSettersAndGetters() {
		int row = 5;
    	int col = 7;
		GamePiece gamePiece = new GamePiece("BLACK");
		GameMatrixCell gameMatrixCell = new GameMatrixCell(row, col, new GamePiece("WHITE"));
		//test setGamePiece
		gameMatrixCell.setGamePiece(gamePiece);
		assertTrue(gamePiece.equals(gameMatrixCell.getGamePiece()));
		//test setValue
		gameMatrixCell.setValue("WHITE");
		assertTrue(gameMatrixCell.getGamePiece().getValue().equals("WHITE"));
		//test getLocation
		assertTrue(gameMatrixCell.getLocation().getCol() == col && gameMatrixCell.getLocation().getRow() == row);
	}

}
