package uk.ac.wlv.cs5006.othellotests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import uk.ac.wlv.cs5006.othello.GamePiece;

class GamePieceTest {

	@Test
	void testConstructor() {
		 boolean white = true;
		    try {
		    	GamePiece gamePiece1 = new GamePiece("WHITE");
		    }catch(Exception e){
		        white = false;
		    }
		    assertTrue(white);

		    boolean black = true;
		    try {
		    	GamePiece gamePiece2 = new GamePiece("BLACK");
		    }catch(Exception e){
		        black = false;
		    }
		    assertTrue(black);
	}
	
	@Test
	void testGetterSetters() {
		String val = "BLACK";
		GamePiece gamePiece = new GamePiece("");
		gamePiece.setValue(val);
		assertEquals(val, gamePiece.getValue());
	}
	 @Test
	 void testEquals() {
		 String val = "BLACK";
		 String val2 = "WHITE";
			GamePiece gamePiece = new GamePiece(val);
			GamePiece gamePiece2 = new GamePiece(val2);
			assertTrue(gamePiece.equals(gamePiece2));
	 }

}
