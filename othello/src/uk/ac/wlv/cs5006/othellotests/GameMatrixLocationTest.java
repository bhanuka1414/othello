package uk.ac.wlv.cs5006.othellotests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import uk.ac.wlv.cs5006.othello.GameMatrixLocation;
import uk.ac.wlv.cs5006.othello.GamePiece;

class GameMatrixLocationTest {

	@Test
	void testDefaultConstructor() {
		 boolean works = true;
		    try {
		    	GameMatrixLocation gameMatrixLocation = new GameMatrixLocation();
		    	assertTrue(gameMatrixLocation.getCol() == -1 && gameMatrixLocation.getRow() == -1);
		    }catch(Exception e){
		    	works = false;
		    }
		    assertTrue(works);
	}
	
	@Test
	void testParameterisedConstructor() {
		 boolean works = true;
		    try {
		    	GameMatrixLocation gameMatrixLocation = new GameMatrixLocation(1,5);
		    	System.out.println(gameMatrixLocation.getCol() + "  "+gameMatrixLocation.getRow());
		    	assertTrue(gameMatrixLocation.getCol() == 5 && gameMatrixLocation.getRow() == 1);
		    }catch(Exception e){
		    	works = false;
		    }
		    assertTrue(works);
	}
	
	@Test
	void testSetInvalid() {
		GameMatrixLocation gameMatrixLocation = new GameMatrixLocation();
		gameMatrixLocation.setInvalid();
		assertTrue(gameMatrixLocation.isInvalid());
	}
	
	@Test
	void testSettersAndGetters() {
		int row = 8;
		int col = 7;
		GameMatrixLocation gameMatrixLocation = new GameMatrixLocation();
		gameMatrixLocation.setRow(row);
		gameMatrixLocation.setCol(col);
		assertTrue(row == gameMatrixLocation.getRow());
		assertTrue(col == gameMatrixLocation.getCol());
	}

}
