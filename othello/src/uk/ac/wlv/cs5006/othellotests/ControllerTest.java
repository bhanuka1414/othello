package uk.ac.wlv.cs5006.othellotests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import uk.ac.wlv.cs5006.othello.Controller;
import uk.ac.wlv.cs5006.othello.GamePiece;
import uk.ac.wlv.cs5006.othello.OthelloModel;

class ControllerTest {

	@Test
	void testAttemptPlay() {
		String val = "BLACK";
		GamePiece gamePiece = new GamePiece(val);
		int row = 4;
    	int col = 4;
    	Controller controller = new Controller(new OthelloModel(8, 8));
    	boolean res = controller.attemptPlay(2, 3, gamePiece);
    	assertTrue(res);
	}

}
